(defproject clj-webtest "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.json "0.2.6"]
                 [compojure "1.5.1"]
                 [hiccup "1.0.5"]
                 [ring/ring-core "1.7.0-RC1"]
                 [ring/ring-json "0.4.0"]
                 [ring/ring-defaults "0.3.2"]
                 [ring/ring-jetty-adapter "1.7.0-RC1"]
                 [ring/ring-devel "1.7.0-RC1"]
                 [http-kit "2.3.0"]]
  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler clj-webtest.handler/app}
  :main clj-webtest.core
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}

   })
