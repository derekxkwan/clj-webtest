(ns clj-webtest.core
  (:require [ring.adapter.jetty :as jetty]
           [clj-webtest.client :as client]
           [clj-webtest.handler :refer [app]]))

(client/test-get-async)
;; jetty is our webserver
(defn main []
  (jetty/run-jetty app {:port 3000}))

(main)

