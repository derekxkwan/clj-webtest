(ns clj-webtest.handler
  (:require [compojure.core :as cj]
            [compojure.route :as route]
            [ring.middleware.json :as mwjson]
            [ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]))

;; compojure is our router for our web app
;; ring abstracts over the jetty http server  and provides middleware (wraps around our basic handler)

(cj/defroutes app-routes
  (cj/GET "/" [] "HI")
  (cj/context "/nest" []
           (cj/GET "/birdy" req
                (println req)
                "birdy!")
           (cj/GET "/rat" req
                (println req)
                "rat!"))
  (cj/POST "/" req
           (let [name (get-in req [:body :name])]
             {:status 200
              :body {:name name}}))
  (route/not-found "Not Found"))

;; -> is the thread first macro, which passes each successive s-expr as the first argt of the next
;; wrap-json-body parses the body of a request with any json as a clojure data type
;; wrap-json-response converts responses with collections into json
;; (so wrap-json-response goes TO json, wrap-json-body goes FROM json
;; wrap-json-params parses params instead of body
;; keywords true makes our json stirng keys into keywords

(def app
  (-> (wrap-reload #'app-routes)
      (mwjson/wrap-json-body {:keywords? true})
      (mwjson/wrap-json-response)
      ;;(wrap-defaults site-defaults)
      ))
