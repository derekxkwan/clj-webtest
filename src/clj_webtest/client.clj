(ns clj-webtest.client
  (:require [org.httpkit.client :as http]
            [clojure.data.json :as json]))

(defn test-get-async []
  (let [opts {:timeout 1000
              :user-agent "clojure-app-test-agent"
              :headers {"Accept" "application/json"}}]
    (http/get "http://httpbin.org/get" opts
              (fn [{:keys [status headers body error]}]
                (if error
                  (println "oops: " error)
                  (let [in-json (json/read-str body :key-fn keyword)
                        origin (:origin in-json)
                        url (:url in-json)]
                    (println in-json)
                    (println (str "origin: " origin ", url: " url)))))
              )))


                
